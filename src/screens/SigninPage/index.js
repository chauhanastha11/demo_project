import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";
import React from "react";
import styles from "./styles";
import SignupPage from "../SignupPage";

const SigninPage = ({ navigation }) => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.firstContainer}>
        <Image
          style={styles.logo}
          source={require("../../assets/logo-1x.png")}
        />
      </View>
      <View style={styles.secondContainer}>
        <Text style={styles.firstHeading}>Welcome Back!</Text>
        <Text style={styles.smallHeading}>Welcome back please enter your details</Text>
        <Text style={styles.secondHeading}>Sign In</Text>

        <View style={styles.thirdContainer}>
          <View style={styles.fourthContainer}>
          <View style={styles.label}>
        {/* <Image style={styles.icon} source={require("../../assets/email1.png")} /> */}
        <Text style={styles.labelText}>Email or Phone</Text>
               </View>        
            <TextInput
              style={styles.input}
              // onChangeText={onChangeNumber}
              //value={number}
              placeholder="Enter your Email or Phone"
              // keyboardType="numeric"
            />
          </View>
          <View style={styles.fourthContainer}>
          <View style={styles.label}>
        {/* <Image style={styles.icon} source={require("../../assets/email1.png")} /> */}
        <Text style={styles.labelText}>Verification Code</Text>
               </View>        
            <TextInput
              style={styles.input}
              // onChangeText={onChangeNumber}
              //value={number}
              placeholder="Enter your verification code"
              // keyboardType="numeric"
            />
          </View>  <View style={styles.fourthContainer}>
          <View style={styles.label}>
        {/* <Image style={styles.icon} source={require("../../assets/email1.png")} /> */}
        <Text style={styles.labelText}>Password</Text>
               </View>        
            <TextInput
              style={styles.input}
              // onChangeText={onChangeNumber}
              //value={number}
              placeholder="*********"
              // keyboardType="numeric"
            />
          </View>
        </View>
        <View  style={{flexDirection:"row", justifyContent:"space-between", padding:10}} >
        <Text style={styles.smallTextStyle}>Remember me</Text>
         
        <TouchableOpacity>
        <Text style={styles.smallLinkStyle}>Forgot Password?</Text>
        </TouchableOpacity>
           </View>
           <View style={styles.signUp}>
           <Text style={styles.signupTextStyle}>Don't have an account? </Text>
           <TouchableOpacity onPress={()=> navigation.navigate("SignupPage")}>
        <Text style={styles.signupLinkStyle}>Sign Up</Text>
        </TouchableOpacity>
        </View>
        <View style={styles.buttonBox}>
        <TouchableOpacity
          onPress={() => navigation.navigate("HomePage")}
          style={styles.button}       
        >
          <Text style={styles.signinTextStyle}>Sign In</Text>
        </TouchableOpacity>
      </View>
    </View>
    </View>
  );
};

export default SigninPage;
