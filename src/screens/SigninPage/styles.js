import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    mainContainer: {
      flex: 1,
      backgroundColor: "#f0fcf3",
    },
    firstContainer: {
      flex: 0.25,
      height: 20,
      width: "100%",
      backgroundColor: "#f0fcf3",
      justifyContent:"center",
      alignItems: "center",
    },
    secondContainer: {
      flex: 0.75,
      height: 80,
      width: "100%",
      backgroundColor: "white",
      borderTopStartRadius: 20,
      borderTopEndRadius: 20
    },
    logo: {
      width: 180,
       height: 130,
      //marginLeft:13,
      resizeMode: "stretch",
      //marginTop: 5
    },
    firstHeading: {
      textAlign: "center",
      padding: 6,
      fontSize: 20,
      fontWeight: "bold",
      color:"#5288a1",
      margin:4,
      marginTop:10
    },
    secondHeading: {
      textAlign: "center",
      padding: 6,
      fontSize: 20,
      fontWeight: "bold",
      marginTop:12
    },
    smallHeading: {
      textAlign: "center",
      fontSize: 17,
      padding: 5,
      //fontWeight:"bold"
    },
    thirdContainer: {
      //flexDirection:"row",
      padding: 10,
    },
    fourthContainer: {
      //flexDirection: "row",
      padding:5,
      paddingHorizontal: 10,
      //alignItems:"center",
      position:"relative",
      margin:5
    },
    label:{
    padding:40,
    //margin:10

    },
    labelText:{
     fontSize:16,
     marginBottom:6,
     fontWeight:"bold"
    },
    input: {
      borderColor: "#d5e1f5",
      //color:"#d5e1f5",
      width: "100%",
      height: 40,
      borderWidth: 1,
      borderRadius: 20,
      padding: 10,
      paddingHorizontal:15,
      fontSize:16
      //paddingLeft:9
    },
    // iconStyle:{
    //   height:40,
    //   width:40,
    //   borderWidth:1,
    //   borderColor:"#d5e1f5",
    //   borderRadius:20,
    //   position:"absolute",
    //  // marginLeft:-4,
    //   backgroundColor:"white",
    //   justifyContent:"center",
    //   alignItems:"center"
    // },  
    icon:{
      width:"50%",
      height:"50%",
      resizeMode:"contain"
    },
    label:{
    },
      //flexDirection:"column"
    buttonBox:{
      //margin:20,
      alignItems:"center"
    },
    button: {
      margin: 30,
      paddingVertical: 2,
       marginHorizontal:30,
      width: 200,
      aspectRatio:4,
      backgroundColor: "orange",
      borderRadius: 15,
      justifyContent: "center",
      alignItems: "center",
    },
    signUp:{
      flexDirection:"row",
      justifyContent:"center",
      marginTop:10
    },
    signupLinkStyle:{
      color:"#388ab0",
      fontSize: 18,
      fontWeight:"bold"
    },
    signupTextStyle: {
      //color: "white",
      // justifyContent:"center",
      textAlign: "center",
      fontSize: 18,
    },
    smallTextStyle:{
      fontSize:15,
    },
    smallLinkStyle:{
      color:"#388ab0",
      fontWeight:"bold"
    },
    signinTextStyle:{
     color: "white",
      justifyContent:"center",
      textAlign: "center",
      fontSize: 20,
      fontWeight:"bold"
    }
  });
  export default styles