import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";
import React from "react";
import styles from "./styles";

const SignupPage = ({ navigation }) => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.firstContainer}>
        <Image
          style={styles.logo}
          source={require("../../assets/logo-1x.png")}
        />
      </View>
      <View style={styles.secondContainer}>
        <Text style={styles.heading}>Sign Up</Text>
        <Text style={styles.smallHeading}>Create your account</Text>

        <View style={styles.thirdContainer}>
          <View style={styles.fourthContainer}>
          <View style={styles.iconStyle}>
          <Image style={styles.icon} source={require("../../assets/email1.png")} />
               </View>        
            <TextInput
              style={styles.input}
              // onChangeText={onChangeNumber}
              //value={number}
              placeholder="Email..."
              // keyboardType="numeric"
            />
          </View>
          <View style={styles.fourthContainer}>
            <View style={styles.iconStyle}>  
            <Image style={styles.icon} source={require("../../assets/varification.png")} />        
               </View>           
            <TextInput
              style={styles.input}
              // onChangeText={onChangeNumber}
              //value={number}
              placeholder="Password..."
              // keyboardType="numeric"
            />
          </View>
          <View style={styles.fourthContainer}>
          <View style={styles.iconStyle}>
          <Image style={styles.icon} source={require("../../assets/phone.png")} />
               </View>        
            <TextInput
              style={styles.input}
              // onChangeText={onChangeNumber}
              //value={number}
              placeholder="+91 123456789"
              // keyboardType="numeric"
            />
          </View>
          <View style={styles.fourthContainer}>
          <View style={styles.iconStyle}>
          <Image style={styles.icon} source={require("../../assets/varification.png")} />
               </View>        
            <TextInput
              style={styles.input}
              // onChangeText={onChangeNumber}
              //value={number}
              placeholder="Mobile Varification Code"
              // keyboardType="numeric"
            />
          </View>
          <Text style={styles.smallHeading}>Fill your invitation code</Text>
          <View style={styles.fourthContainer}>
          <View style={styles.iconStyle}>
          <Image style={styles.icon} source={require("../../assets/email1.png")} />
               </View>        
            <TextInput
              style={styles.input}
              // onChangeText={onChangeNumber}
              //value={number}
              placeholder="Invitaion Code"
              // keyboardType="numeric"
            />
          </View>
          <View style={styles.fourthContainer}>
             <View style={styles.iconStyle}>
             <Image style={styles.icon} source={require("../../assets/varification.png")} />
               </View>        
            <TextInput
              style={styles.input}
              // onChangeText={onChangeNumber}
              //value={number}
              placeholder="Password"
              // keyboardType="numeric"
            />
          </View>
          <View style={styles.fourthContainer}>
          <View style={styles.iconStyle}>
          <Image style={styles.icon} source={require("../../assets/varification.png")} />
               </View>        
            <TextInput
              style={styles.input}
              // onChangeText={onChangeNumber}
              //value={number}
              placeholder="Confirm Password"
              // keyboardType="numeric"
            />
          </View>
        </View>
        <View  style={{flexDirection:"row",justifyContent:"center"}} >
        <Text>I agree with </Text>
          <Text style={{fontWeight:"bold"}}>Terms</Text>
          <Text> and </Text>
           <Text style={{fontWeight:"bold"}}>Conditions</Text>
           </View>
        <View style={styles.buttonBox}>
        <TouchableOpacity
          onPress={() => navigation.navigate("SignupPage")}
          style={styles.button}
        >
          <Text style={styles.textStyle}>Sign-Up</Text>
        </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default SignupPage;
