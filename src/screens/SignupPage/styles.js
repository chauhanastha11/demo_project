import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: "#f0fcf3",
  },
  firstContainer: {
    flex: 0.25,
    height: 20,
    width: "100%",
    backgroundColor: "#f0fcf3",
    justifyContent:"center",
    alignItems: "center",
  },
  secondContainer: {
    flex: 0.75,
    height: 80,
    width: "100%",
    backgroundColor: "white",
    borderTopStartRadius: 20,
    borderTopEndRadius: 20
  },
  logo: {
    width: 180,
     height: 130,
    //marginLeft:13,
    resizeMode: "stretch",
    //marginTop: 5
  },
    heading: {
      textAlign: "center",
      padding: 6,
      fontSize: 19,
      fontWeight: "bold",
    },
    smallHeading: {
      textAlign: "center",
      fontSize: 17,
      padding: 5,
      //fontWeight:"bold"
    },
    thirdContainer: {
      //flexDirection:"row",
      padding: 10,
    },
    fourthContainer: {
      flexDirection: "row",
      padding: 5,
      paddingHorizontal: 10,
      alignItems:"center",
      position:"relative"
    },
    input: {
      borderColor: "#d5e1f5",
      width: "100%",
      height: 40,
      borderWidth: 1,
      borderRadius: 12,
      padding: 10,
      paddingHorizontal:45,
      fontSize:15
      //paddingLeft:9
    },
    iconStyle:{
      height:40,
      width:40,
      borderWidth:1,
      borderColor:"#d5e1f5",
      borderRadius:20,
      position:"absolute",
      marginLeft:-4,
      backgroundColor:"white",
      justifyContent:"center",
      alignItems:"center"
    },  
    icon:{
      width:"50%",
      height:"50%",
      resizeMode:"contain"
    },
    buttonBox:{
      //margin:20,
      alignItems:"center"
    },
    button: {
      margin: 30,
      paddingVertical: 2,
       marginHorizontal:30,
      width: 200,
      aspectRatio:4,
      backgroundColor: "orange",
      borderRadius: 15,
      justifyContent: "center",
      alignItems: "center",
    },
    textStyle: {
      color: "white",
      // justifyContent:"center",
      textAlign: "center",
      fontSize: 22,
    },
  });
  export default styles