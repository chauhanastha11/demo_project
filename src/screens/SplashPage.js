import { StatusBar } from "expo-status-bar";
import React, {useEffect} from "react";
import { StyleSheet, Text, View, Image, Platform } from "react-native";
const SplashPage =(props) =>{
  const {navigation} = props
   useEffect (()=> {
    const timer = setTimeout(()=> {
      navigation.replace('SigninPage');
    }, 1000);
    return timer;
   } ,[]);
  return(
  <View style={styles.logoBox} >
  <Image style={styles.logo} source={require("../assets/logo-1x.png")} />
</View>
  )
}
  export default SplashPage
  const styles = StyleSheet.create({
  logoBox:{
    backgroundColor:"white",
    flex:1,
    alignItems:"center",
    justifyContent:"center"
  },
  logo: {
    width: 210,
    aspectRatio: 1.6,
    //marginLeft:13,
    resizeMode: "stretch",
    //marginTop:50,
    // paddingBottom:9,
    // justifyContent:"center",
    //alignItems:"center"
  },
})