import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, Image, Platform } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import SplashPage from "./src/screens/SplashPage";
import SigninPage from "./src/screens/SigninPage";
import SignupPage from "./src/screens/SignupPage";

const Stack = createNativeStackNavigator();

export default function App(props) {
  const { navigation } = props;
  return (
    <NavigationContainer>
      <Stack.Navigator>        
      <Stack.Screen name="Splash_Page" component={SplashPage} 
options={{
        headerShown:false,
      }}
      />
        <Stack.Screen name="SigninPage" component={SigninPage} 
        options={{
          headerShown:false,
        }}/>
        <Stack.Screen name="SignupPage" component={SignupPage}
        options={{
          headerShown:false,
        }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
    //padding:Platform.OS=="ios"?20:10
    //includeFontPadding:false
  },
  logoBox:{
    backgroundColor:"white",
    flex:1,
    alignItems:"center",
    justifyContent:"center"
  },
  logo: {
    width: 210,
    aspectRatio: 1.6,
    //marginLeft:13,
    resizeMode: "stretch",
    //marginTop:50,
    // paddingBottom:9,
    // justifyContent:"center",
    //alignItems:"center"
  },
});
